# 1. Gestion de projet et documentation

Lors de cette première séance du 16 février 2023 ([1er module](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/project-management-documentation/)), nous avons eu une introduction sur:  
- linux (commandes, historique, open source) ([Linux](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/command-line.md)),  
- la documenation de son travaille (text editors, Writing documentation in Markdown, Typical Markdown to HTML workflow) ([Documentation](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/documentation.md))  
- les principes de gestion de projet (documentation, moduar and hiearchical planning, Supply-Side Time Management, Spiral Development, Triage) ([Project management principles](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/project-management-principles.md))

## Premier pas et installation de Ubuntu
### 1. Téléchargement de Ubuntu 
Pour travailler sous une interface Linux, Ubuntu est un terminal permettant de travailler dans un sous-système sous Linux dans un environement Windows (Windows Subsystem for Linux (WSL)). Le lien de téléchargement est le suivant:  [Microsoft store - Ubuntu](https://www.microsoft.com/store/productId/9PDXGNCFSCZV).  

Un user UNIX name est démandé, le mien : dimdeb (attention sans majuscule, DimDeb ne fonctionne pas).
Un guide d'utilisation de Ubuntu, ainsi que des lignes de commandes à utiliser sont donnés par ce tutoriel : [Canonical - Ubuntu](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview). Il est assez complexe mais bien détaillé. Voici également, un guide des 40 commandes Linux est donné de manière plus réumée par : [Hostinger - commande linux ](https://www.hostinger.com/tutorials/linux-commands). Un autre sous la forme de tableau : [Some Basic UNIX Commands by Donald Hyatt](https://www.tjhsst.edu/~dhyatt/superap/unixcmd.html). Un résumé adapté à la demande ce cours de l'étudiante Pauline Mackelbert de ce cours lors de l'année 2022-2023: [Pauline Mackelbert 1 - Les fondamentaux](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module01/) Elle y résume des bases pour éditer un texte en Mackdown, des commandes sous Linux, le contenu du fichier de configuration mkdocs.yml, le traitement d'images et des videos.

Aprés l'installation de Ubuntu le logo de linux devrait être apparu dans votre explorateur de fichier tout en bas : 
![](../Modules/images/Logo_linux_explorateur.png) 
Hello
### 2. Vérifier la possession des plug-ins, des supports et de leurs derniéres mise à jour. 
Afin de pouvoir écrire en Markdown (.md), des vérifications et installations sont néccessaires. Les instructions à suivre sont détaillées dans le lien suivant : [Mardown - User guide - installation](https://www.mkdocs.org/user-guide/installation/). Néanmoins, voici un résumé avec quelques explications des commandes importantes sous linux. 
Vérifier premièrement que tous les supports sont à jour :  
(car certains logiciels tel que pip pourraient être indiqués inextants, simplement parce qu'ils ne sont pas à jour)
```
#vérifier s'il y a des mises à jour à faire :
dimdeb@Aspire5_Dimitri:~$ sudo apt update
#installer ensuite ces mises à jour :
dimdeb@Aspire5_Dimitri:~$ sudo apt upgrade
```

Vérifier d'avoir la dernière version de Python et pip : 
```
dimdeb@Aspire5_Dimitri:~$ pyhton --version 
#la réponse devrait être la suivante si c'est ok : 
Python 3.10.6 #ou avec d'autre numéro d'une version plus récente  

dimdeb@Aspire5_Dimitri:~$ pip --version 
#la réponse que j'ai eu aprés installation :
pip 22.0.2 from /usr/lib/python3/dist-packages/pip (python 3.10)
```
J'avais oublié de télécharger mkdocs, cela est fait dans le point 5 mais cela peut être déjà réalisé à ce stade comme expliqué dans le lien d'installation.

En cas d'échec, la démarche à suivre pour les installer est la suivante :
``` 
dimdeb@Aspire5_Dimitri:~$ sudo apt install python3
#il vous demandera votre mot de passe linux si nous n'avez pas encore réencodé
dimdeb@Aspire5_Dimitri:~$ sudo apt install python-is-python3 
#pour que l'appel de python revienne à la même chose que python3

dimdeb@Aspire5_Dimitri:~$ sudo apt install python3-pip
```

Vérifier aussi d'avoir Git ([Install GIT](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git))
```
dimdeb@Aspire5_Dimitri:~$ git --version 
```

### 3. Configurer une connexion sécurisée (ssh-keygen) 
Plus d'explications dans ce lien (mais complexe): (Use SSH keys to communicate with GitLab)[https://docs.gitlab.com/ee/user/ssh.html]

Faire générer une clé de sécurité ssh:
```
dimdeb@Aspire5_Dimitri:~$ ssh-keygen
#ensuite:
dimdeb@Aspire5_Dimitri:~$ cat .ssh/id_rsa.pub
```
Insérer dans votre GitLab la clé **SSH** générée et aller chercher sur votre GitLab, le lien suivant:
![](images/git_ssh_screen.png) (git@gitlab.com:fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/dimitri.debauque.git)

Ajouter la commande suivante sur Ubuntu avec ce lien :
```
dimdeb@Aspire5_Dimitri:~$ git@gitlab.com:fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/dimitri.debauque.git^C
```

### 4. Cloner votre GitLab ([Clone a repository](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository))
```
dimdeb@Aspire5_Dimitri:~/repos$ git clone git@gitlab.com:fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/dimitri.debauque.git
```

J'avais créé un dossier repos grâce à cette fonction : 
```
dimdeb@Aspire5_Dimitri:~$ mkdir repos #creer le directrory report
dimdeb@Aspire5_Dimitri:~$ cd repos/ #aller dans ce directory
dimdeb@Aspire5_Dimitri:~/repos$ 
dimdeb@Aspire5_Dimitri:~/.. #revenir au dossier parent 
dimdeb@Aspire5_Dimitri:~$  

dimdeb@Aspire5_Dimitri:~/repos/dimitri.debauque$ cat requirements.txt #pour voir le contenu du dossier
mkdocs
mkdocs-bootswatch
```
### 5. Télécharger le nécessaire pour Markdown
Télécharger les supports nécessaires pour Markdown (ils se trouvent dans le fichier requirement.txt ) 
```
dimdeb@Aspire5_Dimitri:~/repos/dimitri.debauque$ pip3 install -r requirements.txt
#... (downloading)
dimdeb@Aspire5_Dimitri:~/repos/dimitri.debauque$ sudo apt install mkdocs
```


Pour jeter un oeil à votre historique de commandes:
```
dimdeb@Aspire5_Dimitri:~/repos/dimitri.debauque$ history
```

### 6. Configurer son user.mail and user.name
```
git config --global user.email "you@example.com"
git config --global user.name "Your Name"

dimdeb@Aspire5_Dimitri:~/repos/dimitri.debauque$   git config --global user.email "dimitri.debauque@live.fr"
dimdeb@Aspire5_Dimitri:~/repos/dimitri.debauque$   git config --global user.name "DimDeb"
```
Vous êtes maintenant prêt pour modifier de maniére locale votre site GitLab.

### 7. Les commandes GitLab pour charger votre repository 
| Commande                | Description                                            |
|-------------------------|--------------------------------------------------------|
| `git pull`               | Télécharger la dernière version du projet              |
| `git add -A`              | Ajouter tous les changements que nous avons effectués       |
| `git commit -m "message"` | Accompagner d’un message d’explication nos changements |
| `git push`                | Envoyer nos changements sur le serveur                  |

Pour ouvrir vos fichiers localement et les modifier, vous devez posséder un éditeur de texte comme visual code ([download](https://code.visualstudio.com/Download)) qui permet une pré-visualisation, sublime text, ...
Pour ouvrir votre repository, utiliser la commande suivante :
```
dimdeb@Aspire5_Dimitri:~/repos/dimitri.debauque$ code . 
#code pour visual code
#Pour pouvoir en même temps voir un affichage html local entrer la commande suivante:
#(cela permet de mettre la commande code en arrière plan)
dimdeb@Aspire5_Dimitri:~/repos/dimitri.debauque$ code . &
#pour afficer en html sur edge :
dimdeb@Aspire5_Dimitri:~/repos/dimitri.debauque$ mkdocs serve
```
We can put otpion after serve `mkdocs serve [OPTIONS]`:

| Name            | Type    | Description                                                                                           |
|-----------------|---------|-------------------------------------------------------------------------------------------------------|
| -a, --dev-addr  | text    | IP address and port to serve documentation locally (default: localhost:8000)                          |
| --livereload    | text    | Enable the live reloading in the development server (this is the default)                             |
| --no-livereload | text    | Disable the live reloading in the development server.                                                 |
| --dirtyreload   | text    | Enable the live reloading in the development server, but only re-build files that have changed        |
| --watch-theme   | boolean | Include the theme in list of files to watch for live reloading. Ignored when live reload is not used. |


## Écriture en Markdown et quelques outils 
De nombreux outils et résumés ont d'ores et déjà été créés pour expliquer les commandes de bases de Markdown (en plus du résumé de Pauline):  
    1. [Basic Synthax - Markdown guide](https://www.markdownguide.org/basic-syntax)  and [Extended Syntax - Markdown guide](https://www.markdownguide.org/extended-syntax)  
    2. [Syntaxe de base de Markdown - cite de référence](https://fr.markdown.net.br/syntaxe-de-base/), explication des bonnes pratiques et comparaison avec HTML
   
(`#` les titres, `##` sous-titres, `**gras**`, `*italique*`, `***italic et gras***`, subscript `H~2~O`,  Superscript :`x^2^`, insérer une image `![](images/pdp.jpeg)`, un lien `[Microsoft store - Ubuntu](https://www.microsoft.com/store/productId/9PDXGNCFSCZV)`, une annotation[^1] `[^1]`, tableaux, citations `>`, ...)


### Lien 
Pour renvoyer à un lien, tout en modifiant le nom du lien, cela se fait de la manière suivante:

```
[Brussels faculty of engineering (VUB -ULB)](https://bruface.eu/)
#Cela fonctionne aussi pour voyager dans le site en renvoyant vers un des fichier Markdown (.dm) de votre repository
[Page d'intro de mon site](../index.md)  #les ".." signifient retour au dossier parent.
#Une autre methode en html 
<a href="../img/favicon.ico">MkDocs favicon</a>
[dimitri.debauque@ulb.be](mailto:dimitri.debauque@ulb.be)  #mailto est nécessaire

#pour télécharger un fichier 
<a href="/images/myw3schoolsimage.jpg" download>
  <img src="/images/myw3schoolsimage.jpg" alt="W3Schools">
</a>

```
Cela donne ceci:  
[Brussels faculty of engineering (VUB -ULB)](https://bruface.eu/)  
[Page d'intro de mon site](../index.md)  

### Changer logo de la page ([theme custom_dir](https://www.mkdocs.org/user-guide/customizing-your-theme/#using-the-theme-custom_dir))
Rajouter simplement un dossier custom_theme et un dossier img dans celui-ci (custom_im) (attention dans votre repository (pas dans docs)). Rajouter votre logo sous le nom de favicon.**ico**.
Dans le dossier mkdocs.yml, rajouter le :
```
theme:
    name: materia   
    extra:
        include_sidebar: false
    custom_dir: custom_theme
```

### Images 
Pour avoir une bonne fluidité et un chargement rapide de notre page, nos images chargée sur le site doivent être une qualité modéré et adapté.  
Il est d'abord important de comprendre les 2 principals types de fichier image et les types de format leur correspondant:  

- matriciel (chaque pixel est un élement de la matrice image) (image) (JPEG (pour les photos), GIF, PNG(logo sans fond), TIFF, BMP),
    - JPEG on peut optimiser les fichiers JPEG et sélectionner une valeur dpi lors de l'enregistrement d'une image à partir de Photoshop. Les images **JPEG** sont toujours au format carré ou rectangulaire avec un arrière-plan uni. Les images sont principalement utilisées pour la **photographie** de sites Web. JPEG étant un algorithme de compression, les images au format JPEG perdent en qualité afin de réduire leur taille et d'optimiser l'espace de stockage utilisé,  
    - La différence entre les formats **PNG** et JPEG est que les images PNG ont un **fond transparent**. Les fichiers PNG ont généralement une résolution plus élevée. Ces images sont principalement utilisées pour les **logos**, icônes ou autres images affichées avec un fond transparent,  
    - Une caractéristique distinctive du format GIF est que plusieurs photos sont stockées dans une image pour former une vidéo, également appelée **GIF**. Les fichiers GIF peuvent contenir jusqu'à 256 couleurs RVB dans une seule image. Moins vous utilisez de couleurs, plus la taille du fichier est petite. Le format GIF est principalement utilisé pour les **graphiques web**,

- vectoriel (graphique, dessin avec des traits, ...) (SVG, EPS, PDF ou AI),  
    - EPS: utilisé pour sauvegarder des logos, icônes et autres éléments graphiques. Un fichier au format EPS peut être ouvert et modifié avec n’importe quel programme,  
    - SVG: fichier vectoriel sauvegardé en tant que texte XML qui décrit comment les graphiques doivent apparaître. La résolution du fichier est indépendante, tout comme les autres formats vectoriels.  

Source: [Lime Pack](https://www.limepack.be/blog/les-formats-des-fichiers-pour-limpression-3/#:~:text=Le%20format%20matriciel%20ou%20raster%20Les%20graphiques%20raster,une%20cam%C3%A9ra%20ou%20une%20capture%20d%E2%80%99%C3%A9cran%20par%20exemple.)  

Il existe plusieurs logiciel de traitement d'image:  

- privé (Adobe illustrator(graphiques), photoshop(photos)),  

- Open source:  
    - Gimp ([Microsoft store - download](https://apps.microsoft.com/store/detail/XPDM27W10192Q0), [tutoriel](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-filesize-of-a-jpeg)) (compress, resize, crop, batch, ...) C'est une application pour traiter photo par photo. Pour modifier redimensionner la taille d'une image via cet outil, cela peut se faire à partir de `Image -> Taille de l'impression`, comme sur l'image ci-dessous:  
    <img src="../images/1.%20Gimp%20redim.png" alt="1. Gimp" style="width:200px;"/>

    - [GraphicsMagick](http://www.graphicsmagick.org/) fonctionne par ligne de commande. C'est un outil robust de traitement d'image OpeSource. Il fournit une collection efficace d'outils et de bibliothèques qui prennent en charge la lecture, l'écriture et la manipulation d'image dans plus de 92 formats majeurs, y compris des formats importants comme DPX, GIF, JPEG, JPEG-2000, PNG, PDF, PNM, TIFF et WebP. Cela permet d'automatiser le traitement d'images.
    Commandes : 

    | animate   | Animate a sequence of images                                   |
    |-----------|----------------------------------------------------------------|
    | batch     | Executes an arbitary number of utility commands                |
    | benchmark | Measure and report utility command performance.                |
    | compare   | Compare two images using statistics and/or visual differencing |
    | composite | Composite images together                                      |
    | conjure   | Execute a Magick Scripting Language (MSL) XML script           |
    | convert   | Convert an image or sequence of images                         |
    | display   | Display an image on a workstation running X                    |
    | identify  | Describe an image or image sequence                            |
    | import    | Capture an application or X server screen                      |
    | mogrify   | Transform an image or sequence of images                       |
    | montage   | Create a composite image (in a grid) from separate images      |
    | time      | Time the execution of a utility command.                       |
    | version   | Report GraphicsMagick version, features, and build options.    |

    - Pour redimensionner une image et inserer dans le markdown texte une image mais avec une ecriture html:
    `<img src="../images/1.%20Gimp%20redim.png" alt="1. Gimp" style="width:400px;"/>`  
        Pour centrer l'image:  
    `<div style="text-align:center">`  
    `<img src="../images/2.%20SolidWorks%20Empreinte.png" alt="2. empreinte" style="width:400px;"/>  `  
    `</div> `  
    - Pour pouvoir mettre deux images l'un à coté de l'autre:  
    ```
        <div style="text-align:center">  
        <img src="../images/2.%20free%20cad%20esquisse.png" style="float:left;width:200px; "/>    <img src="../images/2.%20free%20cad%20parametrique.png" style="float:left;width:200px;"/>
        </div>   </br>
    ```


## HTML     
Voici un tutoriel sur l'écriture en HTML trés interactif: [w3schools](https://www.w3schools.com/html/default.asp)

[^1]: annotation en bas de page
// File : module2_voiture.scad

//Author : Jonathan Kahan

// Date : 24 Février 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

$fn = 200;

//cube parameters
cube_lar = 50;
cube_long = 50;
cube_h = 10; 

pot_echap_diam = 8;
pot_echap_long = 5;

suspension_hole = 8.2;

antenna_h = 30;
antenna_d = 2;

difference(){
    union(){
        cube(size=[cube_lar, cube_long, cube_h], center = false);
            translate([cube_lar/2, 0,0])
        cylinder(cube_h,d=cube_lar, $fn = 200);

        translate([cube_lar/4,cube_long+pot_echap_long,cube_h/2])
            rotate([90,0,0])
                cylinder(cube_h,d=pot_echap_diam);

        translate([cube_lar/4*3,cube_long+pot_echap_long,cube_h/2])
            rotate([90,0,0])
                cylinder(cube_h,d=pot_echap_diam);

        translate([cube_lar/4*3,cube_long+pot_echap_long,cube_h/2])
            rotate([90,0,0])
                cylinder(cube_h,d=pot_echap_diam);
        
        translate([cube_lar/2,7/10*cube_long, cube_h-5])
            rotate([-30,0,0])
                cylinder(antenna_h, d=antenna_d);

    }
    translate([cube_lar/4,cube_long/4*3,0])
        cylinder(cube_h,d=suspension_hole);
    
    translate([cube_lar/4,0,0])
        cylinder(cube_h,d=suspension_hole);
    
    translate([cube_lar/4*3,0,0])
        cylinder(cube_h,d=suspension_hole);
    
    translate([cube_lar/4*3,cube_long/4*3,0])
        cylinder(cube_h,d=suspension_hole);
}


# 2. Conception Assistée par Ordinateur (CAO) (Computer-Aided Design (CAD))
La page du cours au sujet de ce module  est accessible avec ce [lien](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/computer-aided-design/).  
Comme le titre l'indique, le but de ce module est de connaître différents logiciels de CAO. Évidement, ce cours nous pousse à utiliser des logiciels Open Source tel que Inkscape, Antimony (blocks à connecter similaire à Grasshopper), **OpenSCAD, FreeCAD**. À la suite de cette séance, je me suis d'ailleurs formé à l'utilisation des 2 derniers. Il existe, néanmoins, des logiciels privés tels que Fusion360, Onshape, **Rhino|Grasshopper, SolidWorks**. Les logicels en **gras** sont ceux que j'ai déjà utilisés.

Nous pouvons trouver beaucoups de projets réalisés et donnés en libre accès malheureusement souvent au format stl sur des sites tel que : 
[Thinkgiverse (privé!)](https://www.thingiverse.com/). 


## OpenSCAD 
[Télécharger OpenSCAD ici](https://openscad.org/).  
Le tutoriel du cours de OpenSCAD est [OpenSCAD](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/OpenSCAD.md). L'avantage de ce logiciel est qu'il est uniquement utilisé avec du code. Tout est créé suivant avec un arbre de création formaté suivant un ensemble de fonctions possibles. La cheat sheet reprennant les commandes principales est disponlible suivant ce [lien](https://openscad.org/cheatsheet/). De nombreuses librairies possédant des modules (des fonctions) supplémentaires sont disponibles suivant ce lien: [libraries](https://openscad.org/libraries.html). 
Vous trouverez un exemple ci-dessous de la façon d'utiliser une librairie suivant la documentation disponible.


### Librari - [BOSL](https://github.com/revarbat/BOSL/wiki)

Les instructions à suivre pour télécharger et rendre disponible la librairie dans OpenSCAD sont expliquées dans le lien du titre. La documentation des fonctions disponibles est présentée dans le GitLab s'y référant:
![](images/2%20-%20library%20files.png)
  
Voici un exemple avec la description de la fonction "bez_point()":  
<div style="text-align:center">
<img src="../images/2%20-%20example%20function.png" alt="2. fonctions" style="width:250px;"/>  
</div> 
  
Pour pouvoir utiliser une fonction comme celle-ci se trouvant dans beziers.scad, la demande d'accès doit être encodée:
![](/docs/Modules/images/2%20-%20beziers%2Cscad%20%2B%20library.png)
```
include <BOSL/constants.scad> #toute le temps avec BOSL reférence 
use <BOSL/beziers.scad>
```
Pour comprendre la permière fois comment fonctionne et comment utiliser une librairie, la meilleur est d'ouvrir un exemple. 

#### Pré-Exerice 

##### Module
Voici un exercice que je me suis conçu,  
j'ai créé le module suivant (des fonctions dans un fichier .scad que j'ai mis dans ma librairie "My_modules"):
<div style="text-align:center"> 
<img src="../images/2%20-%20example%20function%20wheel.png" alt="2. example wheel_berk" style="width:450px;"/>  
</div>
##### Dossier "/OPenSCAD/libraries"
Je l'ai inséré dans le dossier `My_modules` que j'ai créé (ma librairie) `/OPenSCAD/libraries/My_modules`:  
![](images/2%20-%20explorateur%20fichiers.png)  

##### Appel de la fonction 
Je peux de la sorte utiliser cette fonction dans un fichier se trouvant dans n'importe quel dossier en appelant le nom du fichier dans lequel se trouve la fonction utilisée:  
<div style="text-align:center">
<img src="../images/2%20-%20wheel_berke.png" alt="2 . Wheel_berk" style="width:500px;"/>
</div>
</br> 
  
### Exercice - Wheel
Pour augmenter la résolution, vous pouvez utiliser ce code:  
```
$fn = 100 #plus la valeur est important, meilleur est la qualité
```  
J'ai créé cette roue "stylisée": 
   
<a href="../images/Wheel.scad" download>
  <img src="../images/2.%20Wheel_3D.png" alt="Wheel download" style="width:300px;">
</a>  
L'ensemble des lignes de codes suivantes sont dans un seul et même fichier. Vous pouvez télécharger mon fichier en cliquant sur la photo ci-dessus.

####  Définitions du projet et des paramétres 
```  
/*
Auteur: Dimitri Debauque
Date de création: 24-02-2023
Nom : Wheel
IP: CC BY-SA 
Institution aquadémique: Université Libre de Bruxelles
Commentaire(s): Réalisé dans le cadre du cours FabZero Experiments
*/

$fn=100;
Diame=10;   //diamétre de la roue
width=3; //épaisseur de la roue
n_hole=4; //#de petit disque/rangé
h_cube=0.5; //#taille des petites étoiles (mutlicarrétourné)
n_row_hole=7;  //#de rangée
t=0.3; // épaisseur raynure
n_ray=3;
```

#### Création des fonctions qui sont appelées dans le code
```
//Module
module Trans_child0(n=1, x=1,y=0,z=0) {     #permet une répétition lineaire 
    for (i = [0 : n-1])     //par défault répition suivant x 
     translate([ x*i, y*i, z*i ]) children(0);
}

module Lenght_division(n=1, l_x=0,l_y=0,l_z=0) { #permet une répétition linéaire de n élement sur une longeur définie
    for (i = [0 : n-1])
     translate([ l_x*i/n, l_y*i/n, l_z*i/n ]) children(0);
}

module Rot_child0(n, arc_angle=360, x=1,y=0,z=0) { #permet une répétition circulaire
   angle_spacing=arc_angle/n;   //par défault répition suivant x 
   for (i = [0 : n-1])
     rotate([ x*angle_spacing*i, y*angle_spacing*i, z*angle_spacing*i ]) children(0);
}

module ring(t=1,d_ext = 10,d_int = 5,x=0,y=0,z=0) {
    //direction vers haut par défault
    rotate([y*90,x*90,0])
    difference() {
        cylinder(h=t, d=d_ext,center=true);
        cylinder(h=t+0.1, d=d_int,center=true);}
}
```
#### Le code
```
//CODE
difference(){//3. Enjoliveur_wheel - Trait
        //3.A
        difference(){//2.Enjoliveur_wheel : wheel - motif intériur
        
            //2.A
            difference(){//1.Wheel : sphere - 2 rectangle
                
                sphere(d=Diame); //1.A
                //1.B
                for(i = [ [ (Diame+width)/4,  0,  0],
                          [-(Diame+width)/4, 0, 0],])
                {   translate(i)    
                    cube([(Diame-width)/2, Diame,Diame],center=true);    }
                }


            //2.B Les motif intérieur : rotation(carrés rotation sur eux meme(5) + translation(n_hole)
            Rot_child0(n=n_row_hole){Trans_child0(n_hole,0,Diame/n_hole/2,0){
                Rot_child0(n=5){cube([width+1,h_cube,h_cube],center=true);}}
        }
    }
    //3.B
    for(l_x1=[width/2,-width/2]){
   Lenght_division(n=n_ray,l_x=l_x1) {
ring(t, d_ext=Diame+0.1, d_int=Diame*0.90, x=1);}}
}
```

### Problèmes rencontrés et tips

- Pour dubliquer un élément tout en le translatant utiliser children(0); attention, je n'ai réussi à faire fonctionner cette commande qu'en la metant elle-même dans une fonction. Prendre exemple sur le module au-dessus.
- le "for" est assez complexe à utiliser, il est très utile de regarder le lien de la cheatsheet. J'ai également utilisé for dans l'exemple ci-dessus. Je n'ai jamais réussi à dupliquer quelque chose en dessous d'un for directement dans le code sans passer par un module comme dans l'exemple.
- La cheatsheet doit être lue attentivement avant de commencer à coder. Il est également important de regarder des exemples et d'aller regarder des solutions déjà réalisées (comme pour tout les logiciels CAD). 

## FreeCAD
Le but de l'utilisation de FreeCad est d'assembler ensemble des pièce de manière également paramètrique pour réaliser un assemblage. Avec Cédric et Jonathan nous voulions créé une voiture, Cédric réalisant la suspansion avec un ressort, Jonathan crée un boite stylisée pour réaliser le chassis. 

J'ai donc voulu réalisé des roues connectés par essieu qui s'assemble comme cela: <div style="text-align:center"><img src="../images/2.%20free%20cad%20coupe%20meca.png" style="width:300px;"/></div> 

J'ai d'abord créé une esquisse, et pour y mettre des valeurs parametriques, on peut par exemple imposer une longeur de la sorte: <div style="text-align:center"> <img src="../images/2.%20free%20cad%20esquisse.png" style="float:left;width:300px; "/><img src="../images/2.%20free%20cad%20parametrique.png" style="width:200px;"/></div> </br> 
J'ai ensuite fait une révolution autour de l'axe Y: <div style="text-align:center"><img src="../images/2.%20freecad%20modele%20femelle.png" style="width:250px;"/>  </div>  </br> 

J'ai construit un cylindre plein avec les mêmes parametres pour obtenir un négatif grâce à une operation booléenne (cut): <div style="text-align:center">      <img src="../images/2.%20%20freecad%20arbre%20de%20cr%C3%A9ation%20avant%20wheel.png" style="width:150px;"/>  </div>  </br> 
Attention, réaliser cette opération dans *l'atelier* "Part" et non dans "part desing": <div style="text-align:center">      <img src="../images/2.%20freecad%20part%20booleen.png" style="width:500px;"/>  </div>  </br> 

Pour permettre un encastrement plus facile, j'ai réalisé un décallage des dimmensions de 0,2 mm: ![](images/2.%20free%20cad%20offeset.png)

J'ai pu également **importer** mon fichier wheel.scad mais malheureusement, la partie paramétrique n'est pas reprise: 
 <div style="text-align:center">  <img src="../images/2.%20free%20cad%20wheel%20arbre.png" style="float:left;width:150px; "/>    <img src="../images/2.%20free%20cad%20wheel%20model.png" style="width:200px;"/>    </div>

## Solidworks 
J'ai réalisé quelques projets grâce à SolidWorks à l'UMons.  
Le projet suivant (2021) reste jusque maintenant (2023) le plus important que j'ai réalisé.
Le but était de comprendre le moyen de production et premettre à quiconque de pouvoir reproduire la pièce suivante:  
![](images/2.%20SolidWorks%20Piece.png)  
  
Une analyse des dimensions, des rugosités des faces, des matériaux, des techniques de productions a été réalisée.  
La pièce modélisée:  
![](images/2.%20SolidWorks%20modele.png)  

Le plan technique (téléchargement.pdf): 
<a href="../images/2%20.%20SolidWorks%20Plan.PDF" download>
  <img src="../images/2.%20SolidWorks%20Plan%20mini.png" alt="Plan download" style="width:150px;">
</a>  

Vu le manque de précision des dimensions et la rugosité importante des faces non usinées, cette pièce a été réalisée par un processus de moulage au sable. Vu que la pièce est creuse, il est nécesssaire de produire 3 noyaux pour la réaliser. Le moulage au sable requiert des angles de dépouilles pour éviter que le sable ne se déplace lors de l'enlèvement des plaques-modèle (modèle comprenant la dilation thermique permettant de donner l'empreinte au sable des moules). Voici l'analyse de la plaque-modèle et de la pièce dilatée termiquement et des noyaux afin de vérifier que chacun possède les bons angles de dépouilles adaptés aux longueurs de frottement:
![](images/2.%20SolidWorks%20Noyaux%20plaque%20mdod%C3%A8le.png)  
Des boîtes à noyaux sont aussi nécessaires pour réaliser les noyaux, également avec les analyses des dépouilles:
![](images/2.%20SolidWorks%20Boite%20%C3%A0%20noyau.png)  
  
L'empreinte dans le sable est représenté ci-dessous, on comprend pourquoi c'est un ensemble de pièces démontables dû à l'enlèvement dans des directions oppossés des pièces formant l'empreinte de la pièce dans le moule en sable: 
<div style="text-align:center">  
    <img src="../images/2.%20SolidWorks%20Empreinte.png" alt="2. empreinte" style="width:400px;"/>  
</div>  

<div style="text-align:center"> 
  <button onclick="playPause()">Play/Pause</button> 
  <button onclick="makeBig()">Big</button>
  <button onclick="makeSmall()">Small</button>
  <button onclick="makeNormal()">Normal</button>
  <br><br>
  <video id="video1" width="600">
    <source src="../images/2.Enregistrement%20assemblages.mp4" type="video/mp4">
    <source src="mov_bbb.ogg" type="video/ogg">
    Your browser does not support HTML video.
  </video>
</div> 

<script> 
var myVideo = document.getElementById("video1"); 

function playPause() { 
  if (myVideo.paused) 
    myVideo.play(); 
  else 
    myVideo.pause(); 
} 

function makeBig() { 
    myVideo.width = 560; 
} 

function makeSmall() { 
    myVideo.width = 320; 
} 

function makeNormal() { 
    myVideo.width = 420; 
} 
</script> 

<p>Video réalisée dans le cadre académique de la formation de bachelier option mécanique à la faculté Polytechnique de Mons.  Tout droits réservés. </a></p>
  
## Fusion 360 
Voir la partie Outil FabLAB CNC


## Rhino | Grasshopper, Kangaroo, Karamba
Voici quelques exemples de travaux réalisés au cours de Parametric design donné à la VUB auquel j'ai suivi en 2022-2023, principalement destinés aux étudiants architectes.
### Grasshopper
![](images/2.%20Rhino%20tower.png)  
![](images/2.%20Rhino%20tower.png)
### Karamba
![](images/2.%20Karamba.png)  


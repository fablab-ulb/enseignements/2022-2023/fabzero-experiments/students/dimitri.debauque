# 3. Impression 3D
La page du cours du module 3, impression 3D, est disponible suivant ce [lien](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/3d-printing/#before-class). La page de tutoriel de conception 3D du fablab est disponible suivant ce [lien](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md).

Nous allons utiliser un "[slicing software](https://en.wikipedia.org/wiki/Slicer_(3D_printing))" : [PrusaSlicer](https://help.prusa3d.com/en/article/install-prusaslicer_1903). Quelques conseils de conception sont donnés par [Prusa](https://help.prusa3d.com/article/modeling-with-3d-printing-in-mind_164135). Le tableau guide de matériaux de Prusa est disponible suivant ce [lien](https://help.prusa3d.com/materials#_ga=2.143417397.1064785333.1677437789-533836946.1677437789). J'ai déjà reçu la formation avec ces mêmes imprimantes à la VUB.
</br></br></br>

# Différentes parties 
Comme expliqué dans le module 2, le but est d'assembler des pièces pour réaliser une petite voiture, en 3 parties:

- Le chassis (Jonathan),
- Le système de suspension (Cédric),
- La roue (Dimi).</br></br>

## Chassis
<div style="text-align:center">
<a href="../images/module2_voiture.scad" download>
  <img src="../images/3.%20BoxVoiture%20mod%C3%A8le.png" alt="Box voiture" style="width:45%;"><img src="../images/3.%20BoxVoiture.png" alt="Box voiture paramétre" style="width:25%;">
</a>  
</div></br>
Vous pouvez cliquer sur l'image pour télécharger le document .scad.  </br></br>

## Suspension 
### Ressort
<div style="text-align:center">
<a href="../images/Spring.fcstd" download>
  <img src="../images/3.%20Ressort%20mod%C3%A8le.png" alt="Ressort" style="width:25%;"><img src="../images/3.%20Ressort%20parametre.png" alt="Ressort paramètres" style="width:45%;">
</a>  
</div></br>
Vous pouvez cliquer sur l'image pour télécharger le document FreeCad.</br>

### Support ressort
![](images/Support_spring.fcstd)
<div style="text-align:center">
<a href="../images/Support_spring.fcstd" download>
  <img src="../images/3.%20support_mod%C3%A8le.png" alt="Support Ressort" style="width:20%;"><img src="../images/3.%20support_parametres.png" alt="support Ressort paramètres" style="width:45%;">
</a>  
</div></br>

## Roue
![roue](images/3.%20roue_modele.png){width=30%}
![roue paramètres](images/3.%20roue_parametres.png){width=60%}  </br></br></br>


# Prusa
Voici l'ensemble de l'assemblage slicé possédant une seule roue pour un premier test:
![Impression assemblage](images/3.%20impression.png){width=100%}  </br></br>
On peut remarquer que la durée d'impression est déjà de 2h25 malgré une disposition proche des pièces. Modifier le taux de remplissage entre 5% et 15% ne change pas grand chose. 
La méthode de remplissage est **giroïde**, cela permet de donner de bonne propriété mécanique dans un maximum de directions.</br></br>
![Réglage d'impression](images/3.%20Prusa%20r%C3%A9glages%20d'impression.png){centre, width=70%}  </br></br>
Aprés avoir exporté en G-code, nous pouvons lancer l'impression. 
Un caligrage et un nettoyage corrects de la platefrome  avant l'impression est vivement conseillé pour une bonne tenue de la futur pièce.
Plus la surface de contact avec la platefrome est petite et plus l'objet est élancé, plus une bordure et des supports sont nécessaires. 

Nous pouvons également changer l'échelle suivant toutes les directions:
![](images/3.%20Prusa%20pi%C3%A8ce%20applatie.png){width=55%}
![](images/3.%20Prusa%20%C3%A9chelle.png){width=40%}</br></br></br>

# Reflexion après la première impression
## Le problème
Le modèle de la roue imprimée requiert une precision trop importante pour l'imprimante, le piston n'est également pas trés fonctionnel:</br>
![](images/3_roue_rat%C3%A9e.jpg){width=48%} 
![](images/3_piston_rat%C3%A9e.jpg){width=48%}
</br></br>

## La nouvelle solution
<div style="text-align:center"> 
  <button onclick="playPause()">Play/Pause</button> 
  <br><br>
  <img src="../images/3_newpiston.png" style="float:left;width:50%; "/>
  <video id="video1" width="40%">
    <source src="../images/3_newPiston.mp4" type="video/mp4">
    <source src="mov_bbb.ogg" type="video/ogg">
    Your browser does not support HTML video.
  </video>
  <br><br>
</div> 

<script> 
var myVideo = document.getElementById("video1"); 
function playPause() { 
  if (myVideo.paused) 
    myVideo.play(); 
  else 
    myVideo.pause(); 
} 
</script> 

![](images/3_4himpression.png){width=70%}





![](images/3_voiture_new.jpg){width=25%}
![](images/3_voiture_new_bottom.jpg){width=38%}
![](images/3_voiture_new_profil.jpg){width=35%}
</br></br></br>




# Télécharger l'ensemble du projet en cliquant sur l'image
<div style="text-align:center">
<a href="../images/Assemblage%20voiture.zip" download>
  <img src="../images/3_new_ensemble_impression.jpg" alt="Assemblage" style="width:70%;">
</a>  
</div>
</br></br></br>

# Support brosse à dent
J'ai trouvé sur [Thingiver](https://www.thingiverse.com/thing:3325238) un support pour ma brosse à dent électrique pour éviter des traces sur l'évier. 
Voici le résultat imprimé (3h30 d'impression Prusa i MK3, remplissage 10% giroïde avec bordure).  
![](images/3_BAD_1.jpg){width=45%} ![](images/3_BAD_2.jpg){width=45%}

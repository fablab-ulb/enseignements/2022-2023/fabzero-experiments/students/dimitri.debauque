# 5. Dynamique de groupe et projet final
Page du cours: [lien](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/team-dynamics-final-project/)  </br></br>


# 5.1. Project Analysis and Design (2/3/23 class)

## 5.1.2. Problem tree
1. Pour créer un "problem tree", mon groupe et moi avons commencé par identifier le problème de base qui nous a rassemblé, c'est-à-dire l'écologie et plus précisément l'E-waste. L'e-waste représente les déchets électroniques et électriques. Cela comprend une large variété de produits que nous jettons après les avoir utilisés qu'ils soient encore fonctionnels ou non. Moins de 40 % des déchets électroniques sont recyclés dans l'UE, ce qui en fait l'un des flux de déchets dont la croissance est la plus forte. La vie moderne est définie par les gadgets électroniques et électriques. Il est difficile d'imaginer vivre sans eux, qu'il s'agisse de smartphones, d'ordinateurs, de machines à laver ou d'aspirateurs. Cependant, les déchets qu'ils produisent compliquent les efforts de l'Union européenne pour réduire leur impact écologique. [E-waste in the EU](https://www.europarl.europa.eu/news/en/headlines/society/20201208STO93325/e-waste-in-the-eu-facts-and-figures-infographic).
1. De nombreux objets sont jetés pour diverses raisons et celles-ci sont rerpises dans les **racines** du problem tree. 
1. Les conséquences sont rerises dans les **branches**.  
Voici notre problem tree:  
![](images/5_Problem.png){width=100%}
</br>

## 5.1.3. Objectif tree
Voici notre objectif tree:  
![](images/5_Objectif.png){width=100%}
</br></br>

# 5.2 Group formation and brainstorming on project problems (7/3/23 class)
Pour ce cours, on nous a demandé de ramener un objet qui nous tenait à coeur sans réellement savoir pourquoi. Le but était de permettre à tout le monde trouver des points communs avec d'autres et des "causes" qui pourrait unir plusieurs personnes.   
Je suis venu avec mon casque bluetooth pour représenter ma "dépendance" (dans le bon sens du therme) à la musique. Je prends ma musique partout avec moi. Cette objet montre aussi mon attrait pour la technologie et l'électronique.  
Notre groupe s'est formé car on a tous ramené un objet électronique. De là est venu l'idée de se baser sur les composant electroniques de nos appareils du quotidient. De plus, nous essayons tous de réparer nos objets du quotidien, j'ai déjà réparé mon ordinateur portable, mon téléphone, machine à laver, ... 
Voici les différentes personnes de mon groupe :  

* Cosmin est étudiant en computer science, c'est un grand bricoleur (répare tout, collectionneur de vieux tracteur qu'il répare, passionné d'électronique et de mécanique). Voici son site FABLAB: [Cosmin Tudor](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/cosmin.tudor/).

* Patrick est en biologie. Il possède le même télephone portable depuis 7 ans, son PC depuis plus longtemps encore, ... Pour lui tout a de la valeur que ce soit dans le domaine électronique ou dans les fruits et légumes. Voici son site: [Patrick Dezseri](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri).

* Louis est en computer science, il déteste les logiciels propiétaires, il aime créer des applications, des sites webs, ... Voici son site: [Louis Vanstappen](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/louis.vanstappen).

* Voici le lien du site de notre groupe:  [Groupe 01](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-01). C'est un petit descriptif complétement subjectif qui ressort de nos discussions et de mes ressentis.  

Vu notre attrait commun pour la réparation, la récupération, l'Open Source et surtout la technologie, on s'est dit qu'on devait trouver un moyen de récupérer des objets électroniques jettés qui possèdent encore beaucoup d'éléments fonctionnels pour pouvoir en faire un nouvel élément et rentrer ainsi dans une démarche écologique de réduction de l'E-Waste. Cette thématique fait partie des 17 objectifs de "[Sustainable development GOALS](https://sdgs.un.org/#goal_section)" des Nations Unies. Tout ce qui est repris dans l'objectif tree et dans le problem tree a fait partie intégrante de notre réflexion pour arriver à cette thématique.
Plus concrétement, par la suite, nous avons eu l'idée de créer un dispositif CNC capable de dessiner avec un bic ou un laser un dessin donné en SVG ou autre. Le but étant d'utiliser le maximum d'objets recyclés, récuperés qui sont produits et jettés en masse. Tout ça en mettant les codes et les explications en Open Source. 
  
# 5.3 Dynamics de group (28/03/23)
Plusieurs axes ont été vu en cours:  


- éviter le rapport de pouvoir(/domination) grâce à une communication structurée. Plusieurs exemples ont été proposé:
    1. Bâton de parole  
    1. Limiter le temps de parole avec l'animateur  
    1. Faire un tour de parole pour être sûr que tout le monde donne son avis  
    1. Quand on veut prendre la parole, mettre son numéro dans la liste d'attente avec ses doigt sur la table  </br></br>

  
- aider la structure de la réunion
    1. choisir un canal de communication  
    1. répartition des tâches  
    1. faire attention à l'art de décider (triangle):  
        1. décision collective   
        1. décision solitaire, (pratique par exemple pour le choix d'un local, cela évite de perdre du temps en réunion)  
        1. ne pas prendre de décision (par exemple quand on n'a pas assez d'information pour prendre une décision et ainsi éviter de perdre du temps)  
    1. prendre des décisions avec des connaissances suffissament mâtures  
    1. avoir une personne chargée de la gestion du temps, une autre : secrétaire, une autre : animateur de réunion  
</br></br>

- comment répartir les tâches:  
    1. tâches relatives aux compétences individuels  
    1. sous-tâches opérationnelles
    1. Animateur: gestion de la communication pour avoir des prises de paroles les plus constructives possibles  
    1. Secrétaire: trace commune des réunions qui permettent de comprendre le bon suivi du projet et pouvoir ainsi prendre de bonnes décisions  
    1. Gestion du temps 

Ces 3 derniers postes peuvent changer idéalement d'une réunion à l'autre  
</br></br>

- avoir une bonne structure de réunion, idée d'un bon déroulement:  
    1. météo d'entrée (check-in/check-out) pour   
        1. comprendre le contexte général, l'attitude collective, l'état de chacun  
        1. déplacer les grosses décisions si nécéssaire   
        1. adapter l'horaire  
    1. Clarifier les rôles  
    1. Citer l'OJ  
    1. Recontextualiser dans l'agenda  
    1. Réunion  
    1. Répartition des tâches  
    1. Météo de sortie  
</br></br>

- la prise de décision et quelques concepts:  
    1. stratégie/opérationnel/gouvernance
    1. argumentation (chemin) avec les infos nécessaires, tableaux de comparaison
    1. vote (majorité)
    1. vote en fonction du domaine de compétence 
    1. tirage au sort
    1. consentement (décision sans objection)
    1. température check, très rapide, vote à la main: 
        1. la main en haut = d'accord 
        1. la main en milieu = sans objection
        1. la main en bas = pas d'accord 
    1. vote pondéré 
    1. vote classé  
</br></br>

- Feedback:
    1. parler en je pour ne pas généraliser: 
        1. je trouve que 
        1. je pense que 
    1. donner des feedbacks collectifs, individuels, positifs, d'encouragement 
    1. pour finir sur une demande si besoin basée sur des FAITS 

Toutes ces techniques sont utilisées de manière générale lors de nos réunions, cela dépend du type de décision et du nombre de participants à la réunion. Nous essayons de prendre la température avant nos réunions et après. La méthode de prise de décision dépend essentiellement du type de décision à prendre. Par exemple, le choix que nous avons dû faire de réaliser une CNC bic et pas laser a été pris avec la température check. Le choix des aspects techniques de la CNC a été réalisé avec un vote dépedant des compétances de chacun. Vu que nous sommes 4, il est assez facile de prendre des décisions mais lorqu'il s'agit de prendre des décisions plus périeuses et surtout de manière efficace nous essayons d'appliquer les techniques proposées lors de ce cours. 
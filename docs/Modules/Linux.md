# Linux : télécharger Ubuntu 

1. Allez ur le site de Ubuntu et télécharger la dernière version du desktop [Ubuntu](https://ubuntu.com/download/desktop#download):

    ![](images_linux/L_1.png){width=100%}

1. Inserer une clés USB de minimum 8Go vide car toute les donnée dessus vont être supprimé par le formatage.

1. Télécharger [Rufus](https://rufus.ie/fr/) (the Reliable USB Formatting Utility (with Source)) (Open source).

1. Ouvrir Rufus dans les téléchargements.
    ![](images_linux/L_2.png){width=100%}


Acceptez les paramètres recommandés. Changez uniquement les paramètres entourés en rouge: sélectionnez la clé USB inséré et sélectionner le dossier Ubuntu.
Lancer le formatage de clés et le clonage du dossier Ubuntu.

1. Redémarer Windows de manière avancé. 
 - Soit an allant dans les paramètres : `Système>Récupération>Démarrage avancé`: 
        ![](images_linux/L_3.png){width=100%}
    - Ou en éteignez votre pc puis rallumer le en cliquant, sur la touche spéficifique (F9 F8, F9, F10, F11, F12, esc) permettant d'atteindre le démarage avancé en même temps que appuyer sur le bouton d'allumage de votre pc. Si vous arrivez au démarrage habituel de windows, c'est que ce n'était pas le bon bouton.
  
   
Le but est d'atteindre le "boot device"

1. Sélectionnez le port USB sur le quel se trouve le téléchargement réalisé:
    ![](images_linux/L_4.png){width=100%}
Le PC va utiliser le system d'exploitation se trouvant sur la clé.

1. Choisi la langue voulu et installe Ubuntu
    ![](images_linux/L_5.png){width=100%}

1. J'ai personnelement installé l'instllation normal et les "third party" qui utilise quand même malheureusement des softwares propriétairse pour avoir un linux assez userfrinedly pour commencer.
    ![](images_linux/L_6.png){width=100%}

1. Choisissez l'emplacement d'installation attention que le formatage d'un volume supprime tout ce qui se trouve à l'intérieur.
    ![](images_linux/L_7.png){width=100%}
Personnlement, j'ai rajouté récenement un disque dur SSD intern, j'ai donc créé un volume spécialement pour Ubuntu:
    ![](images_linux/L_8.png){width=100%}

1. Vous pouvez suivre la procédure de Linux assez simple pour la suite.
1. Pour accéder au boot device de manière automatique, vous pouvez allez dans `Paramètre>Système>Information système>Spécifications de l'appareil(Paramètres avancés du système)>Démarrage et récupération'Paramètres)`:
    ![](images_linux/L_9.png){width=100%}


J'ai pesonnellement suivi les deux vidéos suivantes:
[How To Install Ubuntu And Keep Windows: Dual Boot Tutorial](https://www.youtube.com/watch?v=x1ykDpSzpKU&ab_channel=LinuxTV) et [Ubuntu Complete Beginner's Guide: Download & Installing Ubuntu](https://www.youtube.com/watch?v=W-RFY4LQ6oE&t=113s&ab_channel=LinuxTV)












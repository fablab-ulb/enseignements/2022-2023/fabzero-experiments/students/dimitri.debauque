# 4. Outil Fab Lab sélectionné
# CNC
La page du cours est la suivante: [lien](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/selected-fab-tool/computer-controlled-machining/) Nous avons utlisé [Fusion 360](https://www.autodesk.com/education/edu-software/overview?sorting=featured&filters=individual).

Tout les modèles créés sont stockés en ligne.
## Idée 
Mon but est de créer ce support de téléphone grâce à une fraisseuse CNC:
<div style="text-align:center">      
<img src="../images_CNC/4_1_idee.png" style="width:50%"/>  
</div>  </br> 
</br> </br>

## Conception
### Extrusion
J'ai commencé par réaliser une esquisse:
<div style="text-align:center">      
<img src="../images_CNC/4_2_1_esquisse1.png" style="width:100%"/>  
</div>  </br>
J'ai créé des triangles supplémentaires à la base pour augmenter la stabilité du support.
On peut remarquer que j'ai ajouté des lignes de constructions qui permettent de réduire fortement le nombre de paramètres en utilisant l'option symétrie.
J'ai réalisé une extrusion de cette esquisse pour en faire un volume.  ***Le fond des encoches d'emboitement ne pourra pas posséder d'angle droit mais des congés de rayon minimum égal à celui de la fraise.*** J'ai donc diminiué la largeur des broches d'encastrements de la partie de gauche. (20-d/2=17 ~16,9 mm)

### Enlèvement de matière
J'ai par la suite voulu inscrire mes initiales comme exercice. J'ai, dans un premier temps, réalisé la grande esquissse de l'empreinte sur la surface supérieure du volume qui m'a permis de réaliser l'enlèvement de matière jusqu'à la mi-épaisseur. **Vu que l'usinage ne permet pas de fraiser des angles droits dans les surfaces intérieures, j'ai appliqué des congés grâce à l'option éponyme (congé d'un rayon minimum égal à celui de la fraise).** 
Grâce à l'option décalages des entités, j'ai créé la petite empreinte sur la nouvelle surface intérieure. J'ai vérifié que la fraise pouvait passer entre les 2 D (distance minimale < d_fraise) pour gagner du temps de fraisage. 
<div style="text-align:center">      
<img src="../images_CNC/4_2_2_show.png" style="width:25%"/>  <img src="../images_CNC/4_2_2_esquisse2_distancemin.png" style="width:70%"/>  
</div>  </br>
J'ai réalisé un enlèvement de matière jusqu'à la surface du dessous.
<div style="text-align:center">      
<img src="../images_CNC/4_2_3_1_extrusion.png" style="width:45%"/>  <img src="../images_CNC/4_2_3_2_extrusion.png" style="width:45%"/>  
</div>  </br>

### Congé et pièce finie
J'ai ensuite réalisé un congé sur l'arrête de pose arrière.
<div style="text-align:center">      
<img src="../images_CNC/4_3_volume.png" style="width:80%"/>  
</div>  </br>
</br> </br>


## Fabrication 
### Nouvelle configuration
Pour créer la configuration d'usinage, il faut passer en mode fabrication:
<div style="text-align:center">      
<img src="../images_CNC/4_4_0_fabriquer.png" style="width:100%"/>  
</div>  </br>
et ensuite créé une nouvelle configuration:
<div style="text-align:center">      
<img src="../images_CNC/4_4_1_nouvelle_config.png" style="width:100%"/>  
</div>  </br>

On peut paramétrer le système d'axes qui peut être différent de celui de base de la pièce. Attention, la direction de la flèche z indique du bas vers le haut du brut (cliquer sur "inverser" pour modifier si besoin). Le plus pratique est de définir la plus grande longueur suivant x. On peut définir les dimensions du brut (figure de droite).
<div style="text-align:center">      
<img src="../images_CNC/4_4_2_confi_ref.png" style="width:43%"/>  <img src="../images_CNC/4_4_3_config_brute.png" style="width:54%"/>
</div>  </br>

et sélectionner le point (0;0;0) de référence qu'on définra également en lançant la machine: 
<div style="text-align:center">      
<img src="../images_CNC/4_4_4_point_modele.png" style="width:50%"/>  
</div>  </br> </br>

### Poche intérieure
Sachant qu'on ne souhaite pas réaliser d'étape de finition, on peut décocher "surépaisseur" et réaliser de la sorte une seule étape d'ébauche. Pour ne pas abîmer la fraiseuse, on limite la profondeur de passe à d_fraise/2. On doit également sélectionner l'outil à utiliser (il sera défini par défaut pour les étapes suivantes). La configuration de l'outil est décrite dans la dernière section.
<div style="text-align:center">      
<img src="../images_CNC/4_5_0_poche_surepaisseur_couche_multiple.png" style="width:42%"/>  <img src="../images_CNC/4_5_1_selectionner_outil.png" style="width:31%"/>  
<img src="../images_CNC/4_5_2_outil.png" style="width:100%"/> 
</div> </br> </br>

### Perçage à partir d'une surface usinée
On peut sélectionner la surface usinée dans la section "hauteur sur le dessus" et choisir "la hauteur sur le fond". Dans le cas d'un perçage, on peut usiner un peu (-1mm) dans le martyr (planche de bois aposée sur le plan de découpe de la fraiseuse).
<div style="text-align:center">      
<img src="../images_CNC/4_6_poche_trou_prof_surface.png" style="width:50%"/>  
</div>  </br> </br>


### Vérifier en simulant
<div style="text-align:center">      
<img src="../images_CNC/4_6_simuler_brut.png" style="width:60%"/>  
</div>  </br> </br>


### Congé, rampe 3D 
Plusieurs techniques sont possibles. Pour avoir visualiser les différentes techniques possibles, il faut laisser sa souris longtemps sur les options possibles.
<div style="text-align:center">      
<img src="../images_CNC/4_8_3D_rampes.png" style="width:70%"/>  
</div>  </br> 
Sélection la bonne surface à usiner: 
<div style="text-align:center">      
<img src="../images_CNC/4_8_selection.png" style="width:70%"/>  
</div>  </br>
De multiples paramétres sont définissables.
</br></br>


### Contour, rampe 2D
J'ai modifié la méthode de pénétration de la fraise en passant de l'hélice à la rampe. Ayant préalablement simulé, j'ai choisi le point d'entrée le plus proche de la dernière étape.  
Pour éviter que la pièce bouge lors de la découpe, des onglets sont nécessaires (figure de droite).  
<div style="text-align:center">      
<img src="../images_CNC/4_9_contour_entrée_sortie_rampes_maxz.png" style="width:45%"/>  
<img src="../images_CNC/4_9_onlgets.png" style="width:45%"/>  
</div>  </br>  </br>

### Post traiter 
Pour extraire les infos néccessaire à la machine, on peut post-traiter en fichier texte par exemple.
<div style="text-align:center">      
<img src="../images_CNC/4_post_traiter.png" style="width:70%"/>  
</div>  </br>  </br>  </br>

### Définition de l'outil 
Les catalogues de fraise mentionnent: les dimensions, les vitesses, ... à référencer dans Fusion. On peut ensuite sauvegarder l'outil.
<div style="text-align:center">      
<img src="../images_CNC/4_avnace_de_coupe_1000.png" style="width:45%"/>  <img src="../images_CNC/4_diametre_de_corps.png" style="width:45%"/>  <img src="../images_CNC/4_post_procésseur_file_emplacement.png" style="width:70%"/>
</div>  </br>


## Configuration de la machine 
Au depart la machine est perdu, il faut commencer dans la section job/setup, "to park position". Un pop-up arrive, cliquez sur "home now":
<div style="text-align:center">      
<img src="../images_CNC/4_A_to_park_position.jpg" style="width:100%"/>  
</div>  </br>  </br> 

Positionner la fraise à l'endroit configurer dans avec Fusion, remmtre à zéro X et Y:
<div style="text-align:center">      
<img src="../images_CNC/4_B_set0.jpg" style="width:70%"/>  
</div>  </br>  </br>

Pour Z, positionnez le palpeur en-dessous et allez dans custom et selectionnez le premier fichier:
<div style="text-align:center">      
<img src="../images_CNC/4_D_palpeur_large.jpg" style="width:80%"/>  
</div>  </br>  </br>

<div style="text-align:center">      
<img src="../images_CNC/4_C_custom_setZ.jpg" style="width:100%"/>  
</div>  </br>  </br>

Vous pouvez arrêter la machien quand vous voulez, ouvrant la porte et pour redémarrer appuyer sur play. 

## Tips 

- la machine ne fonctionne et ne peut bouger que si les portes sont fermées.

- attention que si vous paramétrez un brut plus épais que le vrai, les onglets peuvent être insufisant.

- verifiez de pas allez trop profond dans le martyr.

- le fichier texte post_traiter, un bon outil de vérification :
    - premiere ligne : le nom
    - deuxième ligne : l'outil et la **profondeur** de pénétration à vérifier 
<div style="text-align:center">      
<img src="../images_CNC/4_Post_traiter_txt.png" style="width:50%"/>  
</div>  </br>  </br>

- on peut voir le temps d'usinage : 
<div style="text-align:center">      
<img src="../images_CNC/4_temps_usinage.png" style="width:70%"/>  
</div>  </br>  </br>


## Résulats 
La première version est ratée: 
J'ai oublié de changer l'épaisseur insertion de la partie de droite car j'ai du changer d'épaisseur de plaque pour gagner du temps.
<div style="text-align:center">      
<img src="../images_CNC/4_resultat_foire.jpg" style="width:70%"/>  
</div>  </br>  </br>

De plus, j'avais surdimensionné le vrai brut, ma pièce n'était donc plus tenue lors de l'usinage du contour.


Voici, le dernier résulat:
<div style="text-align:center">      
<img src="../images_CNC/4_support_tel_reussi.jpg" style="width:48%"/>  <img src="../images_CNC/4_support_photo.jpg" style="width:48%"/>  
</div>  </br>  </br>

Mon but était de base de la faire avec une planche de 1,5 cm mais le temps d'usinage dépassatit les  15 min demandés lors du cours. La fraise ayant un diametré de 8 mm et les faisant 5,4 mm, le jeu empêche une bonne insertion et coincement.
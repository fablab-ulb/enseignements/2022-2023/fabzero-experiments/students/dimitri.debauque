# Présentation
Bonjour à tous,  
Voici ma page web Fablab,  

Le but de cette page est de décrire, grâce à une documentation de mon travail, l'aquisation de savoirs permettant d'utiliser GitLab, la conception assistée par ordinateur, la découpeuse assistée par ordinateur, l'impression 3D, le prototypage, ... Ces différents apprentissages sont classés et dévellopés sous forme de modules disponibles dans les onglets de ce site.  

Le but de ces modules est de maîtriser la base de ces différents concepts pour permettre la réalisation du projet final (cf. la section projet final).  
Effectivement, un projet final est réalisé par groupe suivant une thématique nous unissant. Dans le cas de mon groupe, la thématique est la réutilisation de composant électronique permettant la diminution d'e-waste. Des outils de gestions d'équipe et de projet nous sont donnés tout au long du projet et documentés dans la même section du site. Cela nous permet de nous remettre en question et d'être de la sorte plus productif.

Ce développement personnel est réalisé le cadre du cours ["How To Make (almost) Any Experiments Using Digital Fabrication" 2022-2023 ULB](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).  

Les travaux des différents autres étudiants sont disponibles suivant ce lien : [GitLab class group](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students). 

# Le site
Mon site comprend deux parties principales:

1. Présentation : la page sur laquelle vous vous trouvez.
1. Les modules sont divisés en plusieurs parties: 
    1. Linux: j'explique comment j'ai installé et alloué une partie de mon disque dur pour installer le système d'exploitation linux en plus de Windows sur mon portable.
    1. Gestion de projet et documentation: j'explique comment installer Ubuntu en sous-système de Windows, comment utiliser GitLab et cloner son repository localement avec une clé de sécurié SSH, écrire en Mardown, en HTML, changer le logo de sa page, une brève expliquation des types d'images et du traitement de celles-ci pour s'adpater au mieux à une page internet.
    1. Conception Assistée par Ordinateur (CAO) (Computer-Aided Design (CAD)) (OpenScad, FreeCAD, Solidworks, Rhino, ...)
    1. Impression 3D avec Prusa
    1. CNC: comment créer une pièce usinée par une CNC (de la description de la conception à l'utilisation de la machine avec quelques tips)
    1. Dynamique de groupe et projet final : quelques conseils de gestion de groupe, de projet et explication de notre projet.

# Qui suis-je?
Je m'appelle Debauque Dimitri, je suis étudiant en master 2 en option construction civile à [Brussels faculty of engineering (VUB -ULB)](https://bruface.eu/). (année 2022-2023)  
J'ai réalisé mon bachelier option mécanique à la [faculté polytechnique de Mons (FPMs)](https://web.umons.ac.be/fpms/fr/) à l'[UMons](https://web.umons.ac.be/fr/). 

![](images/pdp.jpeg)

Aprés l'apprentissage et la prise en main de GitLab et Markdown, j'ai voulu réaliser mon propre site qui fait offfice de CV que j'ai utilisé dans ma recherche d'emploi : [CV- Debauque Dimitri](https://dimdeb.gitlab.io/CV-Dimitri-Debauque/).

## Mes coordonnées
[dimitri.debauque@ulb.be](mailto:dimitri.debauque@ulb.be)  
Mon messenger:  [Dimitri Debauque](https://m.me/dimitri.debauque)  
Mon Linkedln : [Dimitri Debauque](https://www.linkedin.com/in/dimitri-debauque-a167b5234/)  
Si vous avez des questions, besoins de tuyaux, n'hésitez pas à me contacter.

## Liens utiles
Lien de mon cite: [Dimitri Debauque old](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/DimDeb)  
[repo_url](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/dimitri.debauque)  
[site_url](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/dimitri.debauque/)
